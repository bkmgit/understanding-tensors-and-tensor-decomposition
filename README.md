# Understanding Tensors and Tensor Decomposition

Example codes accompanying the posts "Understanding Tensors and Tensor Decomposition" from https://iksinc.online :
- [Part 1](https://iksinc.online/2018/02/12/understanding-tensors-and-tensor-decompositions-part-1/)
- [Part 2](https://iksinc.online/2018/04/03/understanding-tensors-and-tensor-decompositions-part-2/)
- [Part 3](https://iksinc.online/2018/05/02/understanding-tensors-and-tensor-decompositions-part-3/)
